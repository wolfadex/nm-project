import React from 'react';
import { render } from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import App from './components/App';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

const theme = getMuiTheme({
	palette: {
		primary1Color: 'rgb(85, 65, 41)',
	},
});

render(
	<MuiThemeProvider
		muiTheme={theme}
	>
		<App />
	</MuiThemeProvider>,
	document.getElementById('react-root')
);

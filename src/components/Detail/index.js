import React from 'react';

const style = {
	detail: {
		fontSize: '0.9em',
		padding: '0.2em',
	},
	label: {
		marginRight: '0.5em',
	},
};

export const detail = (label, content) =>
	<div
		style={style.detail}
	>
		<b
			style={style.label}
		>
			{label}:
		</b>
		{content}
	</div>;

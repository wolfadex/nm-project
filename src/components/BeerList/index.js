import './index.less';
import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import { detail } from '../Detail';

const styles = {
	beer: {
		normal: {
			margin: '0em 0',
			height: '2em',
		},
		selected: {
			margin: '1em 0',
			height: '10em',
		},
	},
};

class BeerList extends Component {
	constructor(props) {
		super(props);

		this.handleBeerClick = this.handleBeerClick.bind(this);
		this.renderBeer = this.renderBeer.bind(this);

		this.state = {
			selectedBeer: null,
		};
	}

	render() {
		const {
			items = [],
		} = this.props;

		return (
			<ul
				className='beer-list'
			>
				{items.map(this.renderBeer)}
			</ul>
		);
	}

	renderBeer({
		abv,
		available: {
			name: availableName,
		} = {},
		id,
		isOrganic,
		name,
		style: {
			name: styleName,
		} = {},
	}) {
		const {
			selectedBeer,
		} = this.state;
		const isSelected = selectedBeer === id;
		const style = {
			...styles.beer[isSelected ? 'selected' : 'normal'],
		};

		return (
			<li
				key={id}
				onClick={() => this.handleBeerClick(id)}
				className='beer-list__beer'
			>
				<Paper
					rounded={false}
					style={{
						padding: '0 1em',
						margin: `${isSelected ? 1 : 0}em 0`,
						height: `${isSelected ?
								2 + // Base height
								(availableName ? 1.8 : 0) +
								(isOrganic ? 1.8 : 0) +
								(styleName ? 1.8 : 0) +
								(abv ? 1.8 : 0) :
							2}em`,
					}}
					zDepth={isSelected ? 4 : 1}
				>
					<div
						className='beer-list__beer__name'
					>
						{name}
					</div>
					{
						abv &&
						detail(
							'ABV',
							abv,
						)
					}
					{
						isOrganic &&
						detail(
							'Organic',
							isOrganic ? 'Yes' : 'No',
						)
					}
					{
						styleName &&
						detail(
							'Style',
							styleName,
						)
					}
					{
						availableName &&
						detail(
							'Available',
							availableName,
						)
					}
				</Paper>
			</li>
		)
	}

	handleBeerClick(id) {
		this.setState(({ selectedBeer }) => ({
			selectedBeer: selectedBeer === id ? null : id,
		}));
	}
}

export default BeerList;

import './index.less';
import { ipcRenderer } from 'electron';
import React, { Component } from 'react';
import Header from '../Header';
import BeerList from '../BeerList';
import CircularProgress from 'material-ui/CircularProgress';

const styles = {
	loading: {
		left: '50%',
		position: 'absolute',
		top: '25%',
		transform: 'translate(-50%, 0)',
	},
};

class App extends Component {
	constructor(props) {
		super(props);

		this.handleAjaxSuccess = this.handleAjaxSuccess.bind(this);
		this.handleAjaxError = this.handleAjaxError.bind(this);
		this.handleAjaxLoading = this.handleAjaxLoading.bind(this);

		this.state = {
			beers: [],
			error: '',
			loading: false,
		};
	}

	componentDidMount() {
		ipcRenderer.on('ajax-success', this.handleAjaxSuccess);
		ipcRenderer.on('ajax-error', this.handleAjaxError);
		ipcRenderer.on('ajax-loading', this.handleAjaxLoading);
	}

	componentWillUnmount() {
		ipcRenderer.remove('ajax-success', this.handleAjaxSuccess);
		ipcRenderer.remove('ajax-error', this.handleAjaxError);
		ipcRenderer.remove('ajax-loading', this.handleAjaxLoading);
	}

	render() {
		const {
			beers,
			error,
			loading,
		} = this.state;

		return (
			<div>
				<Header />
				{
					loading  &&
					<CircularProgress
						size={80}
						thickness={5}
						style={styles.loading}
					/> ||
					error &&
					<div
						className='error'
					>
						{error}
					</div> ||
					beers.length > 0 &&
					<BeerList
						items={beers}
					/> ||
					<div
						className='make-a-search'
					>
						Please search for a beer
					</div>
				}
			</div>
		);
	}

	handleAjaxSuccess(e, {
		data: {
			data = [],
		} = {},
	}) {
		this.setState((state) => ({
			...state,
			error: null,
			beers: data.sort(({ name: nameA }, { name: nameB }) => nameA > nameB ? 1 : nameA < nameB ? -1 : 0),
			loading: false,
		}));
	}

	handleAjaxError(e, {
		response: {
			errorMessage,
			invalidFields: {
				q: {
					isEmpty,
				} = {},
			} = {},
		} = {},
	}) {
		this.setState((state) => ({
			...state,
			error: isEmpty == null ? errorMessage : 'Please search for a beer',
			beers: [],
			loading: false,
		}));
	}

	handleAjaxLoading() {
		this.setState((state) => ({
			...state,
			loading: true,
		}));
	}
}

export default App;

import './index.less';
import { ipcRenderer } from 'electron';
import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import ActionSearch from 'material-ui/svg-icons/action/search';

class Header extends Component {
	constructor(props) {
		super(props);

		this.handleKeyDown = this.handleKeyDown.bind(this);
		this.handleSearch = this.handleSearch.bind(this);
		this.handleSearchChange = this.handleSearchChange.bind(this);

		this.state = {
			searchText: '',
		};
	}

	render() {
		const {
			searchText,
		} = this.state;

		return (
			<AppBar
				title='Beer Search'
				showMenuIconButton={false}
			>
				<div
					className='search-container'
				>
					<TextField
						hintText='Search...'
						onKeyDown={this.handleKeyDown}
						value={searchText}
						onChange={this.handleSearchChange}
					/>
					<IconButton
						onTouchTap={this.handleSearch}
					>
						<ActionSearch />
					</IconButton>
				</div>
			</AppBar>
		)
	}

	handleSearchChange(e, searchText) {
		this.setState(() => ({
			searchText,
		}));
	}

	handleKeyDown({ keyCode }) {
		// Enter key
		keyCode === 13 && this.handleSearch();
	}

	handleSearch() {
		ipcRenderer.send('ajax-request', this.state.searchText);
	}
}

export default Header;
